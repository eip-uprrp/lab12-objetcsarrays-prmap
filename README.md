
#Lab. 12: Arreglos de Objetos - PRMap



![](http://demo05.cloudimage.io/s/resize/300/i.imgur.com/UnUXImq.png)
![](http://demo05.cloudimage.io/s/resize/350/i.imgur.com/7AneJpf.png)


Los arreglos te permiten guardar y trabajar con varios datos del mismo tipo. Los datos se guardan en espacios de memoria consecutivos a los que se puede acceder utilizando el nombre del arreglo e índices o suscritos que indican la posición en que se encuentra el dato. Es fácil acceder a los elementos de un arreglo utilizando ciclos.

Una tarea bien común en programación usando C++ lo es el trabajar con  arreglos de objetos. En la experiencia de laboratorio de hoy estarás trabajando con datos "georeferenciados" de pueblos en Puerto Rico en donde tendrás atributos como el nombre y la latitud y longitud de su localización que utilizarás para ilustrar propiedades en un mapa.

##Objetivos:

1. Crear dinámicamente y manipular un arreglo de objetos.
2. Codificar funciones para procesar arreglos de objetos.
3. Practicar el pasar arreglos de objetos como parámetros de una función.
4. Practicar la lectura secuencial de datos en un archivo.
5. Usar programación modular.
6. Usar estructuras de repetición y control.


##Pre-Lab:

Antes de llegar al laboratorio debes:

1. haber repasado los conceptos relacionados a arreglos de objetos
2. haber repasado los conceptos relacionados funciones que utilizan arreglos de objetos.

3. haber repasado como leer datos de un archivo.


5. haber estudiado los conceptos e instrucciones de la sesión de laboratorio.

6. haber tomado el [quiz Pre-Lab 12](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=7649) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).

---
---

##Datos "georeferenciados"

Trabajar con arreglos de objetos es una tarea bien común en la programación usando C++. Una vez has leido la información de los objetos en un archivo o provenientes de un usuario, debes depender de tus destrezas algorítmicas y conocimiento sobre C++ para invocar los métodos y funciones adecuadas para procesar los datos correctamente.

En esta experiencia de laboratorio estarás trabajando con datos "georeferenciados" sobre las ciudades en Puerto Rico. El que un dato sea "georeferenciados" quiere decir que el dato tiene una localización física asociada. Típicamente, esta **localización** son coordenadas de longitud y latitud. Por ejemplo, lo que sigue es parte de un archivo que contiene datos georeferenciados de pueblos en Puerto Rico:


```
Arroyo 17.9658 -66.0614
Bayamon 18.3833 -66.15
Caguas 18.2342 -66.0486
Dorado 18.4589 -66.2678
Fajardo 18.3258 -65.6525
```

Como viste en la clase, la manera más común para los programadores en C++ encapsular datos asociados a un ente es utilizando **classes**. Por ejemplo, en el caso de datos georeferenciados, una manera práctica de encapsular la información para cada pueblo sería implementando una clase `GeoreferencedPointOfInterest` que contenga al menos datos (o atributos) para: el nombre del pueblo, su longitud y su latitud. La clase `GOPI` también necesitará implementar métodos para acceder, modificar, y hacer operaciones en sus atributos.

###La clase `GPOI`

En esta experiencia de laboratorio te proveemos una clase `GPOI`  con los siguientes métodos de interfase:


* `GISPOI()`: constructor por defecto

* `GISPOI(QString s, double latitude, double longitude)`:  constructor que recibe nombre, longitud y latitud

* `double getLat()`, `double getLon()`:  "getters" para la latitud y longitud

* ` QString getName()`:  "getter" para el nombre

* `void setAll(string s, double a, double b)`: "setter" para todas las propiedades (a la vez) 

* `double odDistance(const GISPOI &B) const`: dado otro objeto `B` de la clase  `GPOI`, devuelve la distancia *ortodrómica* (*orthodromic*) (la distancia más corta) entre el `GPOI` que invoca y `B`.



##Sesión de laboratorio:

###Ejercicio 0 - Baja y entiende el código

**Instrucciones**


1. Abre un terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/lab12-objecstarrays-prmap.git` para descargar la carpeta `Lab12-ObjectsArrays-prMap` a tu computadora.

2.  Haz doble "click" en el archivo `prMap.pro` para cargar este proyecto a Qt y correrlo. En su estado actual, el programa solo despliega un mapa de Puerto Rico.

    Cuando el programa esté terminado necesitará que el usuario entre texto. Por lo tanto, debemos especificar en `Qt Creator` que este programa debe correr en un **terminal**, no en la ventana `QtCreator`. Para hacer esto, marca el botón de `Projects` en la parte de la izquierda de la ventana de `QtCreator`. Luego selecciona `Run` en la pestaña cerca de la parte de arriba de la ventana. Asegúrate de que la caja `Run in terminal` esté seleccionada.

3. Haz "Build" del programa para que determines si quedan **errores**. Quizás ves algunas advertencias ("warnings") debido a que hay algunas funciones que estean incompletas. Estarás completando estas funciones durante el laboratorio.

4. Abre el archivo `main.cpp`. En este archivo es que estarás escribiendo tu código. El archivo contiene las siguientes funciones:
 
    1. `void printArrayOfCities(GISPOI A[], int size)`: Dado un arreglo `A` de objetos de la clase `GISPOI` y su tamaño, imprime todos los pueblos en el arreglo. Puedes usar esta función como parte de tu proceso de depuración ("debugging").
    
    
    2. `int countLinesInFile(ifstream &file)`: Dada una referencia a un objeto que representa un archivo, esta función cuenta y devuelve el número de filas del archivo.

    3. `void readFileToArray(ifstream &file, GISPOI A[], int numOfCities)`: Dado el objeto `ifstream` de un archivo, un arreglo de pueblos, y el número de registros para leer de un archivo, esta función lee los valores de un archivo y llena el arreglo con objetos. **Esta es una función que tú implementarás**.

    4. `void maxDistances(GISPOI A[], int size, int &idxCityA, int &idxCityB)` : Dado un arreglo `A` de pueblos, determina los dos pueblos que quedan más lejos. La función devuelve (por referencia) los índices de estas ciudades en el arreglo. **Esta es una función que tú implementarás**.

    5. `void minDistances(GISPOI A[], int size, int &idxCityA, int &idxCityB)`: Dado un arreglo `A` de pueblos, determina los dos pueblos que quedan más cerca. La función devuelve (por referencia) los índices de estas ciudades en el arreglo. **Esta es una función que tú implementarás**.
    

    6. `double cycleDistance(GISPOI A[], int size, int P[])`: Dado un arreglo `A` de pueblos, el tamaño de este arreglo, y un arreglo `P` con una permutación de los enteros en `[0,size-1]`, computa y devuelve la distancia de viajar el ciclo de pueblos `A[P[0]]` $\rightarrow$ `A[P[1]]` $\rightarrow \cdots \rightarrow$ `A[P[size-1]]`. 
  
        Por ejemplo, si los pueblos que se leen del archivo fueran  Mayaguez, Ponce, Yauco y San Juan (en ese orden) y la permutación `P` es $\left(3, 1, 0, 2\right)$, la función debe computar la distancia del ciclo San Juan $\rightarrow$ Ponce $\rightarrow$ Mayaguez $\rightarrow$ Yauco $\rightarrow$ San Juan.

        **Esta es una función que tú implementarás**.

Hay otras dos funciones que debes conocer:


1. `void MainWindow::drawLine(const GISPOI &city01, const GISPOI &city02)`: Dada una referencia a dos objetos `GISPOI`, la función pinta una línea entre ellos.

2. `void drawPoints(GISPOI* gisLocations, unsigned int size);`: Dado un arreglo de objetos `GISPOI` y su tamaño, despliega sus localizaciones como puntos en el mapa.

### Ejercicio 1 - lee los puntos georeferenciados a un arreglo 

Recuerda que solo estarás cambiando código en el archivo `main.cpp`. Tu primera tarea será añadir código para leer todo el contenido de un archivo a un arreglo de objetos `GISPOI`.


1. En la función `main()`, añade las instrucciones necesarias para abrir el archivo que contiene la información del pueblo georeferenciado. El archivo está en el directorio `data` y es `pr10.txt`. Necesitas dar el `path` completo del archivo como parámetro del método `open()` de tu objeto `ifstream`. Como siempre, cuando uses archivos debes verificar si el nombre del archivo que pusiste se puede abrir para leer exitosamente.

2. Invoca la función `int countLinesInFile(ifstream &inFile)` para obtener el número de líneas en el archivo. Puedes imprimir el número de líneas obtenido de modo que valides que tu programa está funcionando correctamente.

3. Crea un arreglo **dinámicamente** tan grande como el número de líneas en tu programa.

4. Modifica la función `void readFileToArray(ifstream &file, GISPOI A[], int numOfCities)` de modo que lea todas las líneas en el archivo a objetos en el arreglo.

5. En la función `main()`, invoca la función `readFileToArray`, pasando la referencia al archivo, el arreglo que creaste en el paso 3, y su tamaño.

6. Luego de invocar la función `readFileToArray` puedes invocar la función `void printArrayOfCities(GISPOI A[], int size)` para imprimir los nombres y georeferencias de los puntos leidos del archivo.

7. Pídele al usuario el nombre del archivo de texto que contiene los datos con el siguiente formato: CityName Latitude Longitude. Algunos ejemplos de archivos son:  `cities.txt`, `calif.txt`, y `pr.txt`. 

8. Invoca el método `drawPoints(GISPOI* gisLocations, unsigned int size)` en el objeto `w` de modo que se muestre un punto en el mapa para cada pueblo. La invocación debe hacerse de esta manera: `w.drawPoints(A, size)` (*asumiendo que `A` es el nombre de tu arreglo*). Debes obtener algo parecido a la siguiente figura:

    ![](http://demo05.cloudimage.io/s/resize/400/i.imgur.com/7AneJpf.png) 


### Ejercicio 2 - las funciones max y min 

Una vez que tengas la información de los pueblos georeferenciados en el arreglo de objetos, puedes comenzar a procesarlos de muchas formas interesantes. Comenzaremos con algunas operaciones básicas.

1. Lee la documentación e implementa la la función `void maxDistances(GISPOI A[], int size, int &idxCityA, int &idxCityB)`. Invoca la función desde `main()`.

2. Usa el método `void drawLine(const GISPOI &city01, const GISPOI &city02)` del objeto `w` para dibujar una línea que conecte los pueblos más lejanos. Nota que el segundo y tercer parámetro de este método son **referencias a los objetos que representan los pueblos** (no sus índices en el arreglo).

3. Lee la documentación e implementa la función `void minDistances(GISPOI A[], int size, int &idxCityA, int &idxCityB)`. Invoca la función desde `main()`.

4. Usa el método `void drawLine(const GISPOI &city01, const GISPOI &city02)` del objeto `w` para dibujar una línea que conecte los pueblos más cercanos.


### Ejercicio 3 - computa la distancia del ciclo

1. Lee la documentación e implementa la función `double cycleDistance(GISPOI A[], int size, int P[])`. Invoca la función desde `main()` como se indica en los comentarios dentro de la función `main()`:

    1. Primero con $P = \left(0, 2, 4, 6, 8, 1, 3, 5, 7, 9\right)$,
    2. luego con = \left(0, 3, 6, 9, 1, 4, 7, 2, 5, 8\right)$. 


### Ejercicio 3 - ¡más diversión!

Cambia tu código de modo que ahora abra el archivo `pr.txt`. Valida tus resultados y ¡maravíllate de tu gran logro!



## Entregas:

1. Entrega el archivo `main.cpp` utilizando este [enlace a Moodle](http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=7650).  Recuerda comentar adecuadamente las funciones, usar indentación adecuada y buenas prácticas para darle nombre a las variables.






