#ifndef CITY_H
#define CITY_H

#include <doublepoint.h>
#include <QVector>

///
/// \brief The City class
///        This is merely a vector of points, i.e. the points that make up the
///        polygon of the city.

class City {
public:
    QVector<DoublePoint> *geometry;
    City() {};
    void pushPoint(double x, double y);

    virtual int getSize() { return geometry->size(); }
    QVector<DoublePoint> *getGeometry() { return geometry; }
    ~City() {delete geometry; }
};

#endif // CITY_H
