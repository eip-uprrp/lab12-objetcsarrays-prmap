#ifndef CENSUS_H
#define CENSUS_H

#include <QString>
#include <QMap>

///
/// \brief The Census class
///
class Census : public QMap<QString, double> {

public:
    bool readDataFromFile(QString fileName);
    double minValue, maxValue, factor, negFactor;
};



#endif // CENSUS_H
