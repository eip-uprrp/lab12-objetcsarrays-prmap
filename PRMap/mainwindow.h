#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <census.h>
#include "map.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    Map *myMap;
    void drawPoints(GISPOI* gisLocations, unsigned int size);
    void drawLine(const GISPOI &city01, const GISPOI &city02);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

protected:
    void paintEventOLD(QPaintEvent *event);
};

#endif // MAINWINDOW_H
