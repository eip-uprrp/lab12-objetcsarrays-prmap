#include "gispoi.h"

GISPOI::GISPOI()
{
}

// Function odDistance(A,B)
// Given two objects A and B of type GISPOI, uses their longitudes and
// latitudes to compute and return their orthodromic distance in kilometers.

double GISPOI::odDistance(const GISPOI &B) const {
    return EARTH_RADIUS * acos(
        sin( deg2rad( getLat() ) ) * sin( deg2rad( B.getLat() ) ) +
        cos( deg2rad( getLat() ) ) * cos( deg2rad( B.getLat() ) ) *
        cos( deg2rad( getLon() ) - deg2rad(B.getLon()) ) );
}
