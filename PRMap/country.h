#ifndef COUNTRY_H
#define COUNTRY_H

#include <doublepoint.h>
#include <city.h>
#include <QMap>


///
/// \brief The Country class
///        A map of (city name, city structure)
///
class Country {
public:
    QMap<QString,City*> Cities;

    double minX, minY, maxX, maxY;
    Country() {}
    bool readInfoFromJSON(QString fileName);
    void topLeft(QString &st, DoublePoint &p );
    void limits();
    QMap<QString,City*> getCities() { return Cities;}
    //~Country() { delete Cities; }
};

#endif // COUNTRY_H
