#ifndef MAP_H
#define MAP_H

#include <QWidget>
#include <QMap>
#include <QRgb>
#include <census.h>
#include <country.h>
#include <gispoi.h>
#include <QVector>
#include <QPair>

class Map : public QWidget
{
    Q_OBJECT
public:
    explicit Map(QWidget *parent = 0);
    void setColorMap(QMap<QString,QRgb> *colorMap) {cityColorMap = colorMap;}
    void drawPoints(GISPOI *v, unsigned int size) {gisLocations = v; numLocations = size;}
    void setDrawRoutes(bool r) {drawRoutes = r;}
    void drawLine(const GISPOI &city01, const GISPOI &city02);
    ~Map();
signals:

private:
    Country *myCountry;
    QMap<QString,QRgb> *cityColorMap;
    GISPOI *gisLocations;
    QVector < QPair<const GISPOI *,const GISPOI *> > cityLines;
    unsigned int numLocations;
    bool drawRoutes;

protected:
    void paintEvent(QPaintEvent *event);

public slots:

};

#endif // MAP_H
