#include "country.h"
#include <QFile>
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>

void Country::limits(){
    QMap<QString,City*>::iterator it = Cities.begin();




    minX = maxX = Cities.begin().value()->geometry->at(0).x;
    minY = maxY = Cities.begin().value()->geometry->at(0).y;

    for (it = Cities.begin() + 1; it != Cities.end(); ++it) {

        City *c = it.value();

        for (int i = 0; i < c->getSize(); i++) {
            if (c->geometry->at(i).x != 0.0) {
            if (c->geometry->at(i).x > maxX ) maxX = c->geometry->at(i).x;
            if (c->geometry->at(i).y > maxY ) maxY = c->geometry->at(i).y;
            if (c->geometry->at(i).x < minX ) minX = c->geometry->at(i).x;
            if (c->geometry->at(i).y < minY ) minY = c->geometry->at(i).y;
            }
        }
    }
    qDebug() << minX << minY << maxX << maxY ;
}

void Country::topLeft(QString &st, DoublePoint &p ) {
    QMap<QString,City*>::iterator it = Cities.begin();
    st = it.key();
    p.x = it.value()->getGeometry()->at(0).x;
    p.y = it.value()->getGeometry()->at(0).y;

    qDebug() << p.x << ". " << p.y;
    for (it = Cities.begin(); it != Cities.end(); ++it) {
        QVector<DoublePoint> *geometry = it.value()->getGeometry();
        for (int i = 0; i < geometry->size(); i++) {
            if (geometry->at(i).x > p.x && geometry->at(i).y < p.y)  {
                p.x = geometry->at(i).x;
                p.y = geometry->at(i).y;
                st = it.key();
            }
        }

    }
}
unsigned long  *colorMap;
bool Country::readInfoFromJSON(QString fileName) {
    QFile loadFile(fileName);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return false;
    }

    QByteArray saveData = loadFile.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    QJsonArray topLevelArray = loadDoc.array();

    int ctr = 1;
    foreach(QJsonValue obj, topLevelArray) {
        QVector<DoublePoint> *points = new QVector<DoublePoint>;
        QJsonArray geometryArray = obj.toObject()["geometry"].toObject()["coordinates"].toArray();
        QString polyType = obj.toObject()["geometry"].toObject()["type"].toString();
        if (polyType == "Polygon") {
            for (int i = 0; i < geometryArray[0].toArray().size(); i++) {
                points->push_back(DoublePoint(
                                geometryArray[0].toArray()[i].toArray()[0].toDouble(),
                                geometryArray[0].toArray()[i].toArray()[1].toDouble()));
            }
        }
        else {
            for (int i = 0; i <geometryArray.size() ; i++) {

                for (int j = 0; j < geometryArray[i].toArray()[0].toArray().size(); j++) {
                    points->push_back(DoublePoint(
                                          geometryArray[i].toArray()[0].toArray()[j].toArray()[0].toDouble(),
                                          geometryArray[i].toArray()[0].toArray()[j].toArray()[1].toDouble()));

                }
                // I will push a 0,0 between each polygon of a multipolygon to distinguish
                points->push_back(DoublePoint(0,0));
            }
        }

        QString cityName = obj.toObject()["properties"].toObject()["city"].toString();

        qDebug() << "Storing: " << cityName;
        Cities[cityName] = new City;
        Cities[cityName]->geometry = points;

        ctr++;
    }
}
