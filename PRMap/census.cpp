#include "census.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QDebug>


///
/// \brief
/// Census::readDataFromFile - Populates the Census object with data from a file.
/// at the end we should have Map with (key,value) = (city name, some metric).
/// \param fileName: name of the JSON file
/// \return true if the file was openned and read
///
bool Census::readDataFromFile(QString fileName) {
    QFile loadFile(fileName);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return false;
    }

    QByteArray saveData = loadFile.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    QJsonArray topLevelArray = loadDoc.array();

    int ctr = 1;

    minValue = maxValue = topLevelArray[0].toObject()["percent_change"].toDouble();

    foreach(QJsonValue obj, topLevelArray) {
        double value = obj.toObject()["percent_change"].toDouble();
        (*this)[obj.toObject()["city"].toString()] =  value;
        if (value > maxValue) maxValue = value;
        if (value < minValue) minValue = value;

        ctr++;
    }

    Census::iterator it = this->begin();

    for (; it != this->end(); ++it)
        qDebug() << it.key() << it.value();

    factor = 128.0 / (maxValue - minValue) ;

}

