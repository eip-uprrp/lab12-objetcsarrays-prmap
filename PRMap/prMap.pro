#-------------------------------------------------
#
# Project created by QtCreator 2014-04-21T21:57:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = prMap
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    census.cpp \
    city.cpp \
    country.cpp \
    doublepoint.cpp \
    map.cpp \
    gispoi.cpp

HEADERS  += mainwindow.h \
    census.h \
    city.h \
    country.h \
    doublepoint.h \
    map.h \
    gispoi.h

FORMS    += mainwindow.ui

RESOURCES += \
    prmap.qrc
