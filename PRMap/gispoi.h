#ifndef GISPOI_H
#define GISPOI_H
#include <QString>
#include <iostream>
#include <QDebug>
#include <cmath>
#include "doublepoint.h"
using namespace std;

const double EARTH_RADIUS =  6372.8;
const double TWOPI = 2 * acos(-1);

inline double deg2rad(double deg) {
    return deg/360.0 * TWOPI;
}


class GISPOI
{
private:
    QString name;
    double lat, lon;

public:
    GISPOI();
    GISPOI(QString s, double latitude, double longitude) 
        {name = s; lat = latitude; lon = longitude;}

    double getLat()   const {return lat;}
    double getLon()   const {return lon;}
    QString getName() const {return name;}
    
    void setAll(QString s, double latitude, double longitude)
        {name = s; lat = latitude; lon = longitude;}
    void setAll(string s, double latitude, double longitude) 
        {name = QString::fromStdString(s); lat = latitude; lon = longitude;}
    
    void print() {
        qDebug() << name << " " << lat  << " "
             << lon << endl;
    }

    double odDistance(const GISPOI &B) const;
// Function odDistance(A,B)
// Given two objects A and B of type GISPOI, uses their longitudes and
// latitudes to compute and return their orthodromic distance in kilometers.

 

};





#endif // GISPOI_H
