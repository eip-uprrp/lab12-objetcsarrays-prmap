#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <QPen>
#include <QCoreApplication>
//#include <QJsonDocument>
#include <QFile>
#include <QDebug>
//#include <QJsonArray>
//#include <QJsonObject>
#include <QDesktopWidget>
#include <QVector>
#include <QPoint>
#include <string>
#include <QPolygon>
#include <census.h>
#include <city.h>
#include <country.h>
#include <doublepoint.h>
#include <gispoi.h>

using namespace std;


Country PR;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    srand(time(NULL));
    myMap = new Map(this);
    myMap->show();

    ui->setupUi(this);

    // just hidding the toolbars to have a bigger drawing area.
    QList<QToolBar *> toolbars = this->findChildren<QToolBar *>();
    foreach(QToolBar *t, toolbars) t->hide();
    QList<QStatusBar *> statusbars = this->findChildren<QStatusBar *>();
    foreach(QStatusBar *t, statusbars) t->hide();

    resize(myMap->width(), myMap->height());
}

void MainWindow::drawPoints(GISPOI* gisLocations, unsigned int size) {
    myMap->drawPoints(gisLocations, size);
}

void MainWindow::drawLine(const GISPOI &city01, const GISPOI &city02) {
    myMap->drawLine(city01, city02);
}

MainWindow::~MainWindow()
{
    delete ui;
    if (myMap != NULL) delete myMap;
}

