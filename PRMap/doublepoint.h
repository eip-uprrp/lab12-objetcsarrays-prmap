#ifndef DOUBLEPOINT_H
#define DOUBLEPOINT_H

class DoublePoint {
public:
    double x, y;
    DoublePoint() {x = 0; y = 0;}
    DoublePoint(double a, double b) {
        x = a; y = b;
    }

};

#endif // DOUBLEPOINT_H
